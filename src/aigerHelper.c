
#include <aiger.c>

//from python binding:

void swap_values(void* a, void* b, unsigned size)
{
  char tmp[size];

  memcpy(tmp, a, size);
  memcpy(a, b, size);
  memcpy(b, tmp, size);
}


void aiger_remove_input(aiger* public, unsigned input_lit)
{
  IMPORT_private_FROM (public);
  assert (!aiger_error (public));

  aiger_symbol* input_symbol = aiger_is_input(public, input_lit);
  assert (input_symbol);

  unsigned i;
  for (i = 0; i < public->num_inputs-1; i++)
  {
    aiger_symbol* cur = public->inputs + i;
    if (cur >= input_symbol) //assumes that inputs were allocated on heap..
    {
      swap_values(cur, cur + 1, sizeof(aiger_symbol));

      // we also have to exchange type->idx, which is an address of aiger_symbol within public->inputs

      aiger_type* cur_type = aiger_lit2type(public, cur->lit);
      aiger_type* next_type = aiger_lit2type(public, (cur+1)->lit);
      swap_values(&(cur_type->idx), &(next_type->idx), sizeof(unsigned));
    }
  }
  input_symbol = NULL; //became invalid

  //remove name from symbols table
  aiger_symbol* where_to_del = public->inputs + public->num_inputs-1;
  aiger_delete_symbols_aux(private, where_to_del, 1);

  aiger_type* type = aiger_import_literal (private, input_lit);

  //clean to prevent from being identified as input
  type->and = 0;
  type->input = 0;
  type->latch = 0;

  public->num_inputs--;
}

void
aiger_redefine_input_as_and (aiger* public,
                             unsigned input_lit, unsigned rhs0, unsigned rhs1)
{
  aiger_remove_input(public, input_lit);
  aiger_add_and(public, input_lit, rhs0, rhs1);
}
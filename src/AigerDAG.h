//
// Created by mschledjewski on 5/23/18.
//

#ifndef REACTIVESYNTHESIS_AIGERDAG_H
#define REACTIVESYNTHESIS_AIGERDAG_H

#include <cstdint>
#include <tuple>

//#include <bslstl_pair.h>
#include <bslma_allocator.h>
#include <bslstl_vector.h>
#include <bdlma_pool.h>
#include <bdlma_multipoolallocator.h>
#include <bsls_blockgrowth.h>
#include <bslh_spookyhashalgorithm.h>
#include <bslstl_unorderedmap.h>
#include <spdlog/logger.h>
namespace spd = spdlog;
extern "C" {
#include "aiger.h"
}

struct AigerNode;
struct AigerNodeKeyHasher;



/**
 * TaggedAigerNodePointer uses the least significant bit to store whethere the pointed-to node is used as a negated input.
 */

class TaggedAigerNodePointer
{
  friend AigerNodeKeyHasher;
  AigerNode* ptr;
public:
  TaggedAigerNodePointer& operator =(const TaggedAigerNodePointer& other) = default;
  TaggedAigerNodePointer(): ptr(nullptr){

  }

  explicit TaggedAigerNodePointer(AigerNode* pointer, bool negate = false)
  {
    ptr = negate ? reinterpret_cast<AigerNode*>(reinterpret_cast<uintptr_t>(pointer) ^ static_cast<uint64_t>(1)) : pointer;
  };
  TaggedAigerNodePointer(const TaggedAigerNodePointer& pointer, bool negate = false)
  {
    ptr = negate ? reinterpret_cast<AigerNode*>(reinterpret_cast<uintptr_t>(pointer.ptr) ^ static_cast<uint64_t>(1)) : pointer.ptr;
  };

  AigerNode* operator->(){
    return reinterpret_cast<AigerNode*>(reinterpret_cast<uintptr_t>(ptr) & ~static_cast<uint64_t>(1));
  };

  AigerNode& operator*(){
    return *reinterpret_cast<AigerNode*>(reinterpret_cast<uintptr_t>(ptr) & ~static_cast<uint64_t>(1));
  };

  bool isNegated()
  {
    return (reinterpret_cast<uintptr_t>(ptr) & static_cast<uint64_t>(1))==1;
  }

  AigerNode* getModifiedPointer(){
    return ptr;
  }


  AigerNode* getUnmodifiedPointer(){
    return reinterpret_cast<AigerNode*>(reinterpret_cast<uintptr_t>(ptr)  & ~static_cast<uint64_t>(1));
  }


  bool operator<(const TaggedAigerNodePointer& b) const {
    auto aValue = reinterpret_cast<uintptr_t>(ptr);
    auto bValue = reinterpret_cast<uintptr_t>(b.ptr);
    return aValue < bValue;
  };


  bool operator<=(const TaggedAigerNodePointer& b) const {
    auto aValue = reinterpret_cast<uintptr_t>(ptr);
    auto bValue = reinterpret_cast<uintptr_t>(b.ptr);
    return aValue <= bValue;
  };
  bool operator==(const TaggedAigerNodePointer& b) const {
    return ptr == b.ptr;
  };
};

struct AigerNodeKey
{
  TaggedAigerNodePointer left;
  TaggedAigerNodePointer right;
  //std::int32_t aigerId;
  bool operator==(const AigerNodeKey& b) const {
    return left==b.left && right==b.right;
  };

};


struct AigerNodeKeyHasher{
public:
  std::size_t operator()(const AigerNodeKey& key)
  {
    // TODO soooo many options: https://softwareengineering.stackexchange.com/questions/49550/which-hashing-algorithm-is-best-for-uniqueness-and-speed
    BloombergLP::bslh::SpookyHashAlgorithm hasher;
    hasher(&key.left.ptr,8);
    hasher(&key.right.ptr,8);
    //hasher(&key.aigerId,4);
    return hasher.computeHash();
  }
};


/**
 * A FourCut is an ordered list of pointers to the nodes thar represent the 4-cut.
 *
 * The pointers are ordered and are padded at the back with nullptrs.
 */
/*struct FourCut
{
  AigerNode* zero;
  AigerNode* one;
  AigerNode* two;
  AigerNode* three;
  bool operator ==(const FourCut &other) const
  {
    return zero == other.zero && one == other.one && two == other.two && three == other.three;
  };
};

*/
using FourCut = bsl::vector<AigerNode*>;
//typedef bsl::vector<AigerNode*> FourCut;

/**
 * The AigerNode represent the AIGER DAG.
 *
 * The AigerDAG is the manager of the graph.
 * It also has different allocators:
 *  - a pool allocator for the AigerNode nodes
 *  - a multipool allocator for the vectors of AigerNode::parents and AigerNode::cuts
 */
struct AigerNode{
  AigerNode(TaggedAigerNodePointer left, TaggedAigerNodePointer right, BloombergLP::bdlma::MultipoolAllocator& vectorAllocator, std::int32_t aigerId = -1);
  TaggedAigerNodePointer left;
  TaggedAigerNodePointer right;

  //TODO try llvm small_vec
  bsl::vector<AigerNode*> parents; ///< We need to know the parents, so that we can propagate the 4-cuts changes.
  // TODO also small vec?
  bsl::vector<FourCut> cuts; ///< We precompute all 4-cuts and update them on any changes by propagating changes upwards from the 4 inputs.

  std::uint32_t refCount = 0; ///< During the rewriting we change the refCount and it might temporarily be 0 without the node being deleted.
  std::int32_t aigerId = -1; /**< The ID in the AIGER file. We set this for the Inputs + 0/1 constant. */
  std::uint8_t dfsTraversalId = 0;
  bool isInOriginal; ///< We need to mark the nodes that represent the original circuit, because we cannot change it.
  //bool isInputReplacement; ///< We mark the nodes that replace the controllable inputs. During rewriting we have to update the mapping (controllable input id -> node pointer) because we could merge two nodes that are needed as different controlled input replacements.

};


class AigerDAG {

public:
  AigerDAG();

  TaggedAigerNodePointer addVariableNode(unsigned int varId, bool isControllableInput = false);
  TaggedAigerNodePointer addOriginalAnd(TaggedAigerNodePointer left, TaggedAigerNodePointer right, unsigned int lit);
  TaggedAigerNodePointer addAnd(TaggedAigerNodePointer left, TaggedAigerNodePointer right);

  void setControllableInputFunction(std::int32_t lit, TaggedAigerNodePointer func_as_aiger_node);
  void extractNewAnds(aiger* spec);

    // TODO rewrite

  TaggedAigerNodePointer LOGIC_ZERO; // we should not need this because 0 and 1 can be optimized away

  std::shared_ptr<spd::logger> global_logger;
private:
  bool mergeChildrenCuts(AigerNode& node);
  void optimizeChildAway(AigerNode* node);


  // TODO so many different unordered_map implementations, e.g. https://github.com/sparsehash/sparsehash
  ///< hash map (left+neg,right+neg) -> AigerNode*
  bsl::unordered_map<AigerNodeKey,AigerNode*,AigerNodeKeyHasher> inputs2Node;


  BloombergLP::bdlma::MultipoolAllocator vectorAllocator;
  BloombergLP::bdlma::Pool nodeAllocator;

  bsl::vector<std::tuple<std::int32_t, AigerNode*>> roots; ///< The nodes corresponding to the controllable input functions.
  std::uint8_t currentDfsTraversalId = 0;

};

#endif //REACTIVESYNTHESIS_AIGERDAG_H

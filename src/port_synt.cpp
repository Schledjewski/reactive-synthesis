#include "port_synt.h"
#include <memory>
#include "clara.hpp"
#include <string.h>
#include <stdexcept>
#include <unordered_map>
#include <cuddInt.h>

#include <string.h>
#include <base/abc/abc.h>
#include <base/main/abcapis.h>
#include <base/io/ioAbc.h>

using std::vector;
using std::string;
using std::tuple;
using std::map;
using std::make_tuple;
using std::get;
using std::shared_ptr;
using namespace clara;

std::unordered_map<abc::Abc_Obj_t *, unsigned int>* assignedLiterals;


ABC_NAMESPACE_IMPL_START
#define Abc_AigBinForEachEntry( pBin, pEnt )                   \
    for ( pEnt = pBin;                                         \
          pEnt;                                                \
          pEnt = pEnt->pNext )
#define Abc_AigBinForEachEntrySafe( pBin, pEnt, pEnt2 )        \
    for ( pEnt = pBin,                                         \
          pEnt2 = pEnt? pEnt->pNext: NULL;                     \
          pEnt;                                                \
          pEnt = pEnt2,                                        \
          pEnt2 = pEnt? pEnt->pNext: NULL )

// structural hash table procedures
Abc_Obj_t * Abc_AigAndCreateFrom2( Abc_Aig_t * pMan, Abc_Obj_t * p0, Abc_Obj_t * p1, Abc_Obj_t * pAnd );
void        Abc_AigAndDelete2( Abc_Aig_t * pMan, Abc_Obj_t * pThis );
// incremental AIG procedures
void        Abc_AigReplace_int2( Abc_Aig_t * pMan, Abc_Obj_t * pOld, Abc_Obj_t * pNew, int fUpdateLevel );
void        Abc_AigUpdateLevel_int2( Abc_Aig_t * pMan );
void        Abc_AigUpdateLevelR_int2( Abc_Aig_t * pMan );
void        Abc_AigRemoveFromLevelStructure2( Vec_Vec_t * vStruct, Abc_Obj_t * pNode );
void        Abc_AigRemoveFromLevelStructureR2( Vec_Vec_t * vStruct, Abc_Obj_t * pNode );

unsigned Abc_HashKey22( Abc_Obj_t * p0, Abc_Obj_t * p1, int TableSize )
{
  unsigned Key = 0;
  Key ^= Abc_ObjRegular(p0)->Id * 7937;
  Key ^= Abc_ObjRegular(p1)->Id * 2971;
  Key ^= Abc_ObjIsComplement(p0) * 911;
  Key ^= Abc_ObjIsComplement(p1) * 353;
  return Key % TableSize;
}


struct Abc_Aig_t_
{
  Abc_Ntk_t *       pNtkAig;           // the AIG network
  Abc_Obj_t *       pConst1;           // the constant 1 object (not a node!)
  Abc_Obj_t **      pBins;             // the table bins
  int               nBins;             // the size of the table
  int               nEntries;          // the total number of entries in the table
  Vec_Ptr_t *       vNodes;            // the temporary array of nodes
  Vec_Ptr_t *       vStackReplaceOld;  // the nodes to be replaced
  Vec_Ptr_t *       vStackReplaceNew;  // the nodes to be used for replacement
  Vec_Vec_t *       vLevels;           // the nodes to be updated
  Vec_Vec_t *       vLevelsR;          // the nodes to be updated
  Vec_Ptr_t *       vAddedCells;       // the added nodes
  Vec_Ptr_t *       vUpdatedNets;      // the nodes whose fanouts have changed

  int               nStrash0;
  int               nStrash1;
  int               nStrash5;
  int               nStrash2;
};

/**Function*************************************************************

  Synopsis    [Removes the node from the level structure.]

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
void Abc_AigRemoveFromLevelStructure2( Vec_Vec_t * vStruct, Abc_Obj_t * pNode )
{
  Vec_Ptr_t * vVecTemp;
  Abc_Obj_t * pTemp;
  int m;
  assert( pNode->fMarkA );
  vVecTemp = Vec_VecEntry( vStruct, pNode->Level );
  Vec_PtrForEachEntry( Abc_Obj_t *, vVecTemp, pTemp, m )
  {
    if ( pTemp != pNode )
      continue;
    Vec_PtrWriteEntry( vVecTemp, m, NULL );
    break;
  }
  assert( m < Vec_PtrSize(vVecTemp) ); // found
  pNode->fMarkA = 0;
}

/**Function*************************************************************

  Synopsis    [Removes the node from the level structure.]

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
void Abc_AigRemoveFromLevelStructureR2( Vec_Vec_t * vStruct, Abc_Obj_t * pNode )
{
  Vec_Ptr_t * vVecTemp;
  Abc_Obj_t * pTemp;
  int m;
  assert( pNode->fMarkB );
  vVecTemp = Vec_VecEntry( vStruct, Abc_ObjReverseLevel(pNode) );
  Vec_PtrForEachEntry( Abc_Obj_t *, vVecTemp, pTemp, m )
  {
    if ( pTemp != pNode )
      continue;
    Vec_PtrWriteEntry( vVecTemp, m, NULL );
    break;
  }
  assert( m < Vec_PtrSize(vVecTemp) ); // found
  pNode->fMarkB = 0;
}


/**Function*************************************************************

  Synopsis    [Updates the level of the node after it has changed.]

  Description [This procedure is based on the observation that
  after the node's level has changed, the fanouts levels can change too,
  but the new fanout levels are always larger than the node's level.
  As a result, we can accumulate the nodes to be updated in the queue
  and process them in the increasing order of levels.]

  SideEffects []

  SeeAlso     []

***********************************************************************/
void Abc_AigUpdateLevel_int2( Abc_Aig_t * pMan )
{
  Abc_Obj_t * pNode, * pFanout;
  Vec_Ptr_t * vVec;
  int LevelNew, i, k, v;

  // go through the nodes and update the level of their fanouts
  Vec_VecForEachLevel( pMan->vLevels, vVec, i )
  {
    if ( Vec_PtrSize(vVec) == 0 )
      continue;
    Vec_PtrForEachEntry( Abc_Obj_t *, vVec, pNode, k )
    {
      if ( pNode == NULL )
        continue;
      assert( Abc_ObjIsNode(pNode) );
      assert( (int)pNode->Level == i );
      // clean the mark
      assert( pNode->fMarkA == 1 );
      pNode->fMarkA = 0;
      // iterate through the fanouts
      Abc_ObjForEachFanout( pNode, pFanout, v )
      {
        if ( Abc_ObjIsCo(pFanout) )
          continue;
        // get the new level of this fanout
        LevelNew = 1 + Abc_MaxInt( Abc_ObjFanin0(pFanout)->Level, Abc_ObjFanin1(pFanout)->Level );
        assert( LevelNew > i );
        if ( (int)pFanout->Level == LevelNew ) // no change
          continue;
        // if the fanout is present in the data structure, pull it out
        if ( pFanout->fMarkA )
          Abc_AigRemoveFromLevelStructure2( pMan->vLevels, pFanout );
        // update the fanout level
        pFanout->Level = LevelNew;
        // add the fanout to the data structure to update its fanouts
        assert( pFanout->fMarkA == 0 );
        pFanout->fMarkA = 1;
        Vec_VecPush( pMan->vLevels, pFanout->Level, pFanout );
      }
    }
    Vec_PtrClear( vVec );
  }
}

/**Function*************************************************************

  Synopsis    [Updates the level of the node after it has changed.]

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
void Abc_AigUpdateLevelR_int2( Abc_Aig_t * pMan )
{
  Abc_Obj_t * pNode, * pFanin, * pFanout;
  Vec_Ptr_t * vVec;
  int LevelNew, i, k, v, j;

  // go through the nodes and update the level of their fanouts
  Vec_VecForEachLevel( pMan->vLevelsR, vVec, i )
  {
    if ( Vec_PtrSize(vVec) == 0 )
      continue;
    Vec_PtrForEachEntry( Abc_Obj_t *, vVec, pNode, k )
    {
      if ( pNode == NULL )
        continue;
      assert( Abc_ObjIsNode(pNode) );
      assert( Abc_ObjReverseLevel(pNode) == i );
      // clean the mark
      assert( pNode->fMarkB == 1 );
      pNode->fMarkB = 0;
      // iterate through the fanins
      Abc_ObjForEachFanin( pNode, pFanin, v )
      {
        if ( Abc_ObjIsCi(pFanin) )
          continue;
        // get the new reverse level of this fanin
        LevelNew = 0;
        Abc_ObjForEachFanout( pFanin, pFanout, j )
          if ( LevelNew < Abc_ObjReverseLevel(pFanout) )
            LevelNew = Abc_ObjReverseLevel(pFanout);
        LevelNew += 1;
        assert( LevelNew > i );
        if ( Abc_ObjReverseLevel(pFanin) == LevelNew ) // no change
          continue;
        // if the fanin is present in the data structure, pull it out
        if ( pFanin->fMarkB )
          Abc_AigRemoveFromLevelStructureR2( pMan->vLevelsR, pFanin );
        // update the reverse level
        Abc_ObjSetReverseLevel( pFanin, LevelNew );
        // add the fanin to the data structure to update its fanins
        assert( pFanin->fMarkB == 0 );
        pFanin->fMarkB = 1;
        Vec_VecPush( pMan->vLevelsR, LevelNew, pFanin );
      }
    }
    Vec_PtrClear( vVec );
  }
}

/**Function*************************************************************

  Synopsis    [Deletes an AIG node from the hash table.]

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
void Abc_AigAndDelete2( Abc_Aig_t * pMan, Abc_Obj_t * pThis )
{
//  std::cout << "remove " << reinterpret_cast<uintptr_t >(pThis) <<"\n";
  assignedLiterals->erase(pThis);
  Abc_Obj_t * pAnd, * pAnd0, * pAnd1, ** ppPlace;
  unsigned Key;
  assert( !Abc_ObjIsComplement(pThis) );
  assert( Abc_ObjIsNode(pThis) );
  assert( Abc_ObjFaninNum(pThis) == 2 );
  assert( pMan->pNtkAig == pThis->pNtk );
  // get the hash key for these two nodes
  pAnd0 = Abc_ObjRegular( Abc_ObjChild0(pThis) );
  pAnd1 = Abc_ObjRegular( Abc_ObjChild1(pThis) );
  Key = Abc_HashKey22( Abc_ObjChild0(pThis), Abc_ObjChild1(pThis), pMan->nBins );
  // find the matching node in the table
  ppPlace = pMan->pBins + Key;
  Abc_AigBinForEachEntry( pMan->pBins[Key], pAnd )
  {
    if ( pAnd != pThis )
    {
      ppPlace = &pAnd->pNext;
      continue;
    }
    *ppPlace = pAnd->pNext;
    break;
  }
  assert( pAnd == pThis );
  pMan->nEntries--;
  // delete the cuts if defined
  if ( pThis->pNtk->pManCut )
    Abc_NodeFreeCuts( pThis->pNtk->pManCut, pThis );
}


/**Function*************************************************************

  Synopsis    [Performs canonicization step.]

  Description [The argument nodes can be complemented.]

  SideEffects []

  SeeAlso     []

***********************************************************************/
Abc_Obj_t * Abc_AigAndCreateFrom2( Abc_Aig_t * pMan, Abc_Obj_t * p0, Abc_Obj_t * p1, Abc_Obj_t * pAnd )
{
  Abc_Obj_t * pTemp;
  unsigned Key;
  assert( !Abc_ObjIsComplement(pAnd) );
  // order the arguments
  if ( Abc_ObjRegular(p0)->Id > Abc_ObjRegular(p1)->Id )
    pTemp = p0, p0 = p1, p1 = pTemp;
  // create the new node
  Abc_ObjAddFanin( pAnd, p0 );
  Abc_ObjAddFanin( pAnd, p1 );
  // set the level of the new node
  pAnd->Level      = 1 + Abc_MaxInt( Abc_ObjRegular(p0)->Level, Abc_ObjRegular(p1)->Level );
  pAnd->fExor      = Abc_NodeIsExorType(pAnd);
  // add the node to the corresponding linked list in the table
  Key = Abc_HashKey22( p0, p1, pMan->nBins );
  pAnd->pNext      = pMan->pBins[Key];
  pMan->pBins[Key] = pAnd;
  pMan->nEntries++;
  // create the cuts if defined
//    if ( pAnd->pNtk->pManCut )
//        Abc_NodeGetCuts( pAnd->pNtk->pManCut, pAnd );
  pAnd->pCopy = NULL;
  // add the node to the list of updated nodes
//    if ( pMan->vAddedCells )
//        Vec_PtrPush( pMan->vAddedCells, pAnd );
  return pAnd;
}


void Abc_AigDeleteNode2( Abc_Aig_t * pMan, Abc_Obj_t * pNode );
/**Function*************************************************************

  Synopsis    [Replaces one AIG node by the other.]

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
void Abc_AigReplace2( Abc_Aig_t * pMan, Abc_Obj_t * pOld, Abc_Obj_t * pNew, int fUpdateLevel )
{
  assert( Vec_PtrSize(pMan->vStackReplaceOld) == 0 );
  assert( Vec_PtrSize(pMan->vStackReplaceNew) == 0 );
  Vec_PtrPush( pMan->vStackReplaceOld, pOld );
  Vec_PtrPush( pMan->vStackReplaceNew, pNew );
  assert( !Abc_ObjIsComplement(pOld) );
  // process the replacements
  while ( Vec_PtrSize(pMan->vStackReplaceOld) )
  {
    pOld = (Abc_Obj_t *)Vec_PtrPop( pMan->vStackReplaceOld );
    pNew = (Abc_Obj_t *)Vec_PtrPop( pMan->vStackReplaceNew );
    Abc_AigReplace_int2( pMan, pOld, pNew, fUpdateLevel );
  }
  if ( fUpdateLevel )
  {
    Abc_AigUpdateLevel_int2( pMan );
    if ( pMan->pNtkAig->vLevelsR )
      Abc_AigUpdateLevelR_int2( pMan );
  }
}

/**Function*************************************************************

  Synopsis    [Performs internal replacement step.]

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
void Abc_AigReplace_int2( Abc_Aig_t * pMan, Abc_Obj_t * pOld, Abc_Obj_t * pNew, int fUpdateLevel )
{
  Abc_Obj_t * pFanin1, * pFanin2, * pFanout, * pFanoutNew, * pFanoutFanout;
  int k, v, iFanin;
  // make sure the old node is regular and has fanouts
  // (the new node can be complemented and can have fanouts)
  assert( !Abc_ObjIsComplement(pOld) );
  assert( Abc_ObjFanoutNum(pOld) > 0 );
  // look at the fanouts of old node
  Abc_NodeCollectFanouts( pOld, pMan->vNodes );
  Vec_PtrForEachEntry( Abc_Obj_t *, pMan->vNodes, pFanout, k )
  {
    if ( Abc_ObjIsCo(pFanout) )
    {
      Abc_ObjPatchFanin( pFanout, pOld, pNew );
      continue;
    }
    // find the old node as a fanin of this fanout
    iFanin = Vec_IntFind( &pFanout->vFanins, pOld->Id );
    assert( iFanin == 0 || iFanin == 1 );
    // get the new fanin
    pFanin1 = Abc_ObjNotCond( pNew, Abc_ObjFaninC(pFanout, iFanin) );
    assert( Abc_ObjRegular(pFanin1) != pFanout );
    // get another fanin
    pFanin2 = Abc_ObjChild( pFanout, iFanin ^ 1 );
    assert( Abc_ObjRegular(pFanin2) != pFanout );
    // check if the node with these fanins exists
    if ( (pFanoutNew = Abc_AigAndLookup( pMan, pFanin1, pFanin2 )) )
    { // such node exists (it may be a constant)
      // schedule replacement of the old fanout by the new fanout
      Vec_PtrPush( pMan->vStackReplaceOld, pFanout );
      Vec_PtrPush( pMan->vStackReplaceNew, pFanoutNew );
      continue;
    }
    // such node does not exist - modify the old fanout node
    // (this way the change will not propagate all the way to the COs)
    assert( Abc_ObjRegular(pFanin1) != Abc_ObjRegular(pFanin2) );

    // if the node is in the level structure, remove it
    if ( pFanout->fMarkA )
      Abc_AigRemoveFromLevelStructure2( pMan->vLevels, pFanout );
    // if the node is in the level structure, remove it
    if ( pFanout->fMarkB )
      Abc_AigRemoveFromLevelStructureR2( pMan->vLevelsR, pFanout );

    // remove the old fanout node from the structural hashing table
    Abc_AigAndDelete2( pMan, pFanout );
    // remove the fanins of the old fanout
    Abc_ObjRemoveFanins( pFanout );
    // recreate the old fanout with new fanins and add it to the table
    Abc_AigAndCreateFrom2( pMan, pFanin1, pFanin2, pFanout );
    assert( Abc_AigNodeIsAcyclic(pFanout, pFanout) );

    if ( fUpdateLevel )
    {
      // schedule the updated fanout for updating direct level
      assert( pFanout->fMarkA == 0 );
      pFanout->fMarkA = 1;
      Vec_VecPush( pMan->vLevels, pFanout->Level, pFanout );
      // schedule the updated fanout for updating reverse level
      if ( pMan->pNtkAig->vLevelsR )
      {
        assert( pFanout->fMarkB == 0 );
        pFanout->fMarkB = 1;
        Vec_VecPush( pMan->vLevelsR, Abc_ObjReverseLevel(pFanout), pFanout );
      }
    }

    // the fanout has changed, update EXOR status of its fanouts
    Abc_ObjForEachFanout( pFanout, pFanoutFanout, v )
      if ( Abc_AigNodeIsAnd(pFanoutFanout) )
        pFanoutFanout->fExor = Abc_NodeIsExorType(pFanoutFanout);
  }
  // if the node has no fanouts left, remove its MFFC
  if ( Abc_ObjFanoutNum(pOld) == 0 )
    Abc_AigDeleteNode2( pMan, pOld );
}

/**Function*************************************************************

  Synopsis    [Performs internal deletion step.]

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
void Abc_AigDeleteNode2( Abc_Aig_t * pMan, Abc_Obj_t * pNode )
{
  Abc_Obj_t * pNode0, * pNode1, * pTemp;
  int i, k;

  // make sure the node is regular and dangling
  assert( !Abc_ObjIsComplement(pNode) );
  if(!static_cast<bool>(Abc_ObjIsNode(pNode))){
    return;
  }
  assert( Abc_ObjIsNode(pNode) );
  assert( Abc_ObjFaninNum(pNode) == 2 );
  assert( Abc_ObjFanoutNum(pNode) == 0 );

  // when deleting an old node that is scheduled for replacement, remove it from the replacement queue
  Vec_PtrForEachEntry( Abc_Obj_t *, pMan->vStackReplaceOld, pTemp, i )
    if ( pNode == pTemp )
    {
      // remove the entry from the replacement array
      for ( k = i; k < pMan->vStackReplaceOld->nSize - 1; k++ )
      {
        pMan->vStackReplaceOld->pArray[k] = pMan->vStackReplaceOld->pArray[k+1];
        pMan->vStackReplaceNew->pArray[k] = pMan->vStackReplaceNew->pArray[k+1];
      }
      pMan->vStackReplaceOld->nSize--;
      pMan->vStackReplaceNew->nSize--;
    }

  // when deleting a new node that should replace another node, do not delete
  Vec_PtrForEachEntry( Abc_Obj_t *, pMan->vStackReplaceNew, pTemp, i )
    if ( pNode == Abc_ObjRegular(pTemp) )
      return;

  // remember the node's fanins
  pNode0 = Abc_ObjFanin0( pNode );
  pNode1 = Abc_ObjFanin1( pNode );

  // add the node to the list of updated nodes
  if ( pMan->vUpdatedNets )
  {
    Vec_PtrPushUnique( pMan->vUpdatedNets, pNode0 );
    Vec_PtrPushUnique( pMan->vUpdatedNets, pNode1 );
  }

  // remove the node from the table
  Abc_AigAndDelete2( pMan, pNode );
  // if the node is in the level structure, remove it
  if ( pNode->fMarkA )
    Abc_AigRemoveFromLevelStructure2( pMan->vLevels, pNode );
  if ( pNode->fMarkB )
    Abc_AigRemoveFromLevelStructureR2( pMan->vLevelsR, pNode );
  // remove the node from the network
  Abc_NtkDeleteObj( pNode );

  // call recursively for the fanins
  if ( Abc_ObjIsNode(pNode0) && pNode0->vFanouts.nSize == 0 )
    Abc_AigDeleteNode2( pMan, pNode0 );
  if ( Abc_ObjIsNode(pNode1) && pNode1->vFanouts.nSize == 0 )
    Abc_AigDeleteNode2( pMan, pNode1 );
}

ABC_NAMESPACE_IMPL_END

namespace {

bool is_negated(unsigned l) {
  return (l & 1) == 1;
}

unsigned strip_lit(unsigned l) {
  return (l & ~1);
}

unsigned int negate(unsigned int lit) {
  return lit ^ 01;
}


DdNodePtr complement(DdNodePtr lit) {
  return reinterpret_cast<DdNodePtr>(reinterpret_cast<uintptr_t>(lit) ^ 01);
}

std::string trim(const std::string& str,
                 const std::string& whitespace = " \t")
{
  const auto strBegin = str.find_first_not_of(whitespace);
  if (strBegin == std::string::npos)
    return ""; // no content

  const auto strEnd = str.find_last_not_of(whitespace);
  const auto strRange = strEnd - strBegin + 1;

  return str.substr(strBegin, strRange);
}


DYNAMIC_REORDERING getDynamicReordering(const std::string& strategy){
  if(strategy.empty()){
    return DYNAMIC_REORDERING::CUDD_REORDER_SIFT;
  }

  if(strategy == "CUDD_REORDER_SIFT"){
    return DYNAMIC_REORDERING::CUDD_REORDER_SIFT;
  }
  if(strategy == "CUDD_REORDER_SIFT_CONVERGE"){
    return DYNAMIC_REORDERING::CUDD_REORDER_SIFT_CONVERGE;
  }
  if(strategy == "CUDD_REORDER_SYMM_SIFT"){
    return DYNAMIC_REORDERING::CUDD_REORDER_SYMM_SIFT;
  }
  if(strategy == "CUDD_REORDER_SYMM_SIFT_CONV"){
    return DYNAMIC_REORDERING::CUDD_REORDER_SYMM_SIFT_CONV;
  }
  if(strategy == "CUDD_REORDER_GROUP_SIFT"){
    return DYNAMIC_REORDERING::CUDD_REORDER_GROUP_SIFT;
  }
  if(strategy == "CUDD_REORDER_GROUP_SIFT_CONV"){
    return DYNAMIC_REORDERING::CUDD_REORDER_GROUP_SIFT_CONV;
  }
  if(strategy == "CUDD_REORDER_LAZY_SIFT"){
    return DYNAMIC_REORDERING::CUDD_REORDER_LAZY_SIFT;
  }

  throw std::invalid_argument("Unknown dynamic reordering strategy: "+strategy);

}

bool isComplemented(DdNode* pointer){
  return (reinterpret_cast<uintptr_t>(pointer) & 01) == 1;
}

}



struct WinningRegionTranslator
{
  WinningRegionTranslator(aiger *spec,
                            Cudd &cudd);
  ~WinningRegionTranslator();

  aiger* winningRegionAiger;
  Cudd& cudd;
  std::unordered_map<DdNode*,unsigned int> availableAndsInputsLatches;
  std::unordered_map<unsigned int, unsigned int> index2latchLiteral;
  unsigned int nextLit = 0;


  unsigned int translate(DdNodePtr nodeIn);
  unsigned int add_and(unsigned int lhs, unsigned int rhs);
};

unsigned int WinningRegionTranslator::translate(DdNodePtr node)
{
  unsigned int latchLit = index2latchLiteral[Cudd_NodeReadIndex(node)];

  const auto isComplementedPointer = isComplemented(node);
  if(isComplementedPointer)
  {
    node = complement(node);
  }


  auto result = availableAndsInputsLatches.find(node);
  if(result != availableAndsInputsLatches.end()){
    if(isComplementedPointer)
    {
      return negate(result->second);
    }
    else
    {
      return result->second;
    }
  }


  // a_bdd = ite(a, then_bdd, else_bdd)
  // = a*then + !a*else
  // = !(!(a*then) * !(!a*else))
  // -> in general case we need 3 more ANDs

  auto thenLit = translate(Cudd_T(node));
  auto elseLit = translate(Cudd_E(node));

  auto leftAnd = add_and(latchLit,thenLit);
  auto rightAnd = add_and(negate(latchLit),elseLit);

  auto nandLit = negate(add_and(negate(leftAnd),negate(rightAnd)));

  availableAndsInputsLatches.emplace(node,nandLit);
  return isComplementedPointer ? negate(nandLit) : nandLit;
}


WinningRegionTranslator::WinningRegionTranslator(aiger *spec,
                                                 Cudd &cuddI) : cudd(cuddI),availableAndsInputsLatches(),index2latchLiteral() {
  winningRegionAiger = aiger_init();
  // copy the latches as inputs
  const auto numLatches = spec->num_latches;
  nextLit = 0;
  for (int i = 0; i < numLatches; ++i) {
    const auto latchLit = spec->latches[i].lit;
    if(latchLit>nextLit)
    {
      nextLit = latchLit;
    }
    const auto nodePtr = cudd.bddVar(latchLit).getNode();
    index2latchLiteral.emplace(Cudd_NodeReadIndex(nodePtr),latchLit);
    aiger_add_input(winningRegionAiger, latchLit, "");
    availableAndsInputsLatches.emplace(nodePtr, latchLit);
  }
  nextLit+=2;

  const auto oneNodePtr = cudd.bddOne().getNode();
  availableAndsInputsLatches.emplace(oneNodePtr, 1);
  availableAndsInputsLatches.emplace(complement(oneNodePtr), 0);
  index2latchLiteral.emplace(Cudd_NodeReadIndex(oneNodePtr),1);
}
WinningRegionTranslator::~WinningRegionTranslator() {
  aiger_reset (winningRegionAiger);
}
unsigned int WinningRegionTranslator::add_and(unsigned int lhs, unsigned int rhs) {
  if (lhs == 0 || rhs == 0) {
    return 0;
  } else {
    if (lhs == 1) {
      return rhs;
    } else {
      if (rhs == 1) {
        return lhs;
      } else {
        auto and_lit = nextLit;
        nextLit+=2;
        aiger_add_and(winningRegionAiger, and_lit, lhs, rhs);
        return and_lit;
      }
    }
  }

}

void printWinningRegion(const BDD &winningRegion, aiger *spec, Cudd &cudd, FILE *stream)
{
  std::fputs("\nWINNING_REGION\n",stream);
  WinningRegionTranslator translator(spec, cudd);

  // do the actual translation
  aiger_add_output(translator.winningRegionAiger,translator.translate(winningRegion.getNode()),"winning region");
  aiger_write_to_file(translator.winningRegionAiger, aiger_ascii_mode, stream);

}


int main (int argc, char *argv[]) {

#ifdef NDEBUG
  auto deadline = std::chrono::system_clock::now() + std::chrono::minutes(59);
#else
  auto deadline = std::chrono::system_clock::now() + std::chrono::seconds(50);
#endif

  bool realizabilityOnly = false;
  std::string aigerFile;
  std::string outFile;
  bool fullCircuit = false;
  string reordering= "CUDD_REORDER_SIFT";
  string strategyExtraction = "optimized";
  string quantification;

  auto cli
      = Opt(fullCircuit)
      ["--full"]
          ("produce a full circuit that has outputs other than error bit")
          | Opt(outFile, "outputFileName")
          ["-o"]["--out"]
              ("output file in AIGER format (if realizable)")
          | Opt(realizabilityOnly)
          ["-r"]["--realizability"]
              ("Check Realizability only (do not produce circuits)")
          | Opt(reordering,"reordering")
          ["-d"]["--dynamic-reorder"]
              ("Enables dynamic reordering, set to ON or one of the CUDD strategies")
          | Opt(strategyExtraction,"strategyExtraction")
          ["-e"]["--StrategyExtraction"]
              ("strategy extraction: standard|optimized|optimized_cofactor")
          | Opt(quantification,"quantification")
          ["-q"]["--quantification"]
              ("quantification method: standard (Exist/UnivAbstract)| and (Andabstract)")
          | Arg(aigerFile, "aiger")
              ("input specification in AIGER format");
  auto parseResult = cli.parse(Args(argc, argv));
  if (!parseResult) {
    std::cerr << "Error in command line: " << parseResult.errorMessage() << std::endl;
    std::cerr << cli << std::endl;
    exit(EXIT_FAILURE);
  }



  if (aigerFile.empty()) {
    std::cerr << "Error in command line: no aiger file specified" << std::endl;
    std::cerr << cli << std::endl;
    exit(EXIT_FAILURE);
  }




  DYNAMIC_REORDERING dynamicReordering = getDynamicReordering(reordering);
  SYNTHESIZE_TASK synthesizeTask= realizabilityOnly ? SYNTHESIZE_TASK::REACHABILITY_ONLY : SYNTHESIZE_TASK::SYNTHESIZE;
  OUTPUT_AIGER introduce_output= fullCircuit ? OUTPUT_AIGER::ADD_OUTPUT : OUTPUT_AIGER::NO_NEW_OUTPUT;

  STRATEGY_EXTRACTION strategyExtractionAlgorithm;
  {
    bool validStrategyExtraction = false;
    if (strategyExtraction == "standard") {
      validStrategyExtraction = true;
      strategyExtractionAlgorithm = STRATEGY_EXTRACTION::STANDARD;
    }
    if (strategyExtraction == "optimized") {
      validStrategyExtraction = true;
      strategyExtractionAlgorithm = STRATEGY_EXTRACTION::OPTIMIZED;
    }
    if (strategyExtraction == "optimized_cofactor") {
      validStrategyExtraction = true;
      strategyExtractionAlgorithm = STRATEGY_EXTRACTION::OPTIMIZED_BLOEM;
    }
    if (strategyExtraction.empty()) {
      validStrategyExtraction = true;
      strategyExtractionAlgorithm = STRATEGY_EXTRACTION::OPTIMIZED;
    }
    if (!validStrategyExtraction) {
      std::cerr << "Error in command line: the strategy extraction has to be one of the following standard|optimized|optimized_cofactor  but was " << strategyExtraction << std::endl;
      std::cerr << cli << std::endl;
      exit(EXIT_FAILURE);
    }

  }





    try {
      Synt synthesizer
          {dynamicReordering, strategyExtractionAlgorithm, synthesizeTask, introduce_output, aigerFile, outFile,deadline};
      return synthesizer.run();
    } catch (const spd::spdlog_ex &ex) {
      std::cerr << "Log initialization failed: " << ex.what() << std::endl;
    }


  return EXIT_FAILURE;
}

aiger_symbol* Synt::get_err_symbol(){
  if(!((spec->num_outputs == 1) ^ (spec->num_bad == 1))){
    if constexpr (ENABLE_LOGGING) {
        global_logger->critical("not a safety property");
    }
    exit(EXIT_FAILURE);
  }

  if(spec->num_outputs == 1)
    return spec->outputs;

  return spec->bad;
}

Synt::Synt(DYNAMIC_REORDERING dynamicReordering,
           STRATEGY_EXTRACTION strategyExtractionAlgorithm,
           SYNTHESIZE_TASK realizabilityOnly,
           OUTPUT_AIGER introduce_output,
           const std::string &aigerFile,
           const std::string &outFile,
           std::chrono::system_clock::time_point &deadline) :
    dynamicReordering(dynamicReordering),
    strategyExtractionAlgorithm(strategyExtractionAlgorithm), checkRealizabilityOnly(realizabilityOnly),
    introduce_output(introduce_output), outFile(outFile) ,bddCache(),deadline(deadline){

  // set up the logger
  std::vector<spd::sink_ptr> sinks;
  auto color_sink =
      std::make_shared<spdlog::sinks::ansicolor_stdout_sink_st>();
  sinks.push_back(color_sink);
  auto daily_sink =
      std::make_shared<spd::sinks::daily_file_sink_st>("logfile.txt", 23, 59);
  sinks.push_back(daily_sink);
  global_logger = std::make_shared<spd::logger>("combinedLogger", begin(sinks), end(sinks));
  spd::register_logger(global_logger);
  global_logger->flush_on(spd::level::warn);
  global_logger->set_level(
      spd::level::info);


  // read in the aiger file
  parse_into_spec(aigerFile.c_str());

  assignedLit.reserve(2*(spec->num_ands+spec->num_inputs+spec->num_latches));
  assignedLiterals = &assignedLit;

  manager = cudd.getManager();
  cudd.bddVar(2*(spec->num_inputs + spec->num_latches)); // resizes to the needed size (primed) (guess)

  switch (dynamicReordering) {
  case DYNAMIC_REORDERING::CUDD_REORDER_SIFT: cudd.AutodynEnable(CUDD_REORDER_SIFT); break;
  case DYNAMIC_REORDERING::CUDD_REORDER_SIFT_CONVERGE: cudd.AutodynEnable(CUDD_REORDER_SIFT_CONVERGE); break;
  case DYNAMIC_REORDERING::CUDD_REORDER_SYMM_SIFT: cudd.AutodynEnable(CUDD_REORDER_SYMM_SIFT); break;
  case DYNAMIC_REORDERING::CUDD_REORDER_SYMM_SIFT_CONV: cudd.AutodynEnable(CUDD_REORDER_SYMM_SIFT_CONV); break;
  case DYNAMIC_REORDERING::CUDD_REORDER_GROUP_SIFT: cudd.AutodynEnable(CUDD_REORDER_GROUP_SIFT); break;
  case DYNAMIC_REORDERING::CUDD_REORDER_GROUP_SIFT_CONV: cudd.AutodynEnable(CUDD_REORDER_GROUP_SIFT_CONV); break;
  case DYNAMIC_REORDERING::CUDD_REORDER_LAZY_SIFT: cudd.AutodynEnable(CUDD_REORDER_LAZY_SIFT); break;
  }

  bddCache.emplace(0,cudd.bddZero());
  abc::Abc_Start();
//  abc::Abc_UtilsSource(  abc::Abc_FrameGetGlobalFrame() );
  abcAigerDAG =  abc::Abc_NtkAlloc( abc::ABC_NTK_STRASH, abc::ABC_FUNC_AIG, 1 );
  auto oneNode = abc::Abc_AigConst1(abcAigerDAG);
//  if(Abc_ObjIsComplement(oneNode)){
//    availableAigerNodes.emplace(cudd.bddZero().getNode(),Abc_ObjNot(oneNode)); // we should not need this because 0 and 1 can be optimized away
//  } else {
    availableAigerNodes.emplace(cudd.bddOne().getNode(),oneNode); // we should not need this because 0 and 1 can be optimized away
//  }
  assignedLit.emplace(oneNode,1);

  markInputsAndCategorizeControlledUncontrolledInputs(); // populates the inputs in the cache
  compose_init_state_bdd(); // populates the latches in the cache
  createTransitionCompositionVector(); // populates the ands in the cache

  aiger_symbol* error_symbol = get_err_symbol();
  auto errorBDD = bddForAigerLit(error_symbol->lit);
  const auto node = errorBDD.getNode();
  auto errorOutput = abc::Abc_NtkCreatePo(abcAigerDAG);
//  abc::Abc_ObjAssignName( errorOutput, "errorOutput", nullptr);
  if(isComplemented(node)){
    abc::Abc_ObjAddFanin( errorOutput, abc::Abc_ObjNot(availableAigerNodes.at(complement(node))));
  } else{
    abc::Abc_ObjAddFanin( errorOutput, availableAigerNodes.at(node) );
  }
  not_error_bdd = !errorBDD;
}

Synt::~Synt() {
  aiger_reset (spec);
  delete [] transitionCompositionArray;
  abc::Abc_NtkDelete( abcAigerDAG);
  abc::Abc_Stop();
}


int Synt::run() {
  auto [realizable, func_by_var, winningRegion] = synthesize();
  if(!realizable){
    if constexpr (ENABLE_LOGGING) {
      global_logger->info("unrealizable");
    }
    std::puts("UNREALIZABLE");
    return EXIT_STATUS_UNREALIZABLE;
  }

std::puts("REALIZABLE");
  if (checkRealizabilityOnly == SYNTHESIZE_TASK::REACHABILITY_ONLY ) {
    if constexpr (ENABLE_LOGGING) {
      global_logger->info("realizable");
    }
    return EXIT_STATUS_REALIZABLE;
  }

//  if constexpr (ENABLE_LOGGING) {
//    global_logger->info("realizable");
//  }
//  cudd.AutodynDisable(); // keep pointers stable
  int numAnds = spec->num_ands;
  {
    {
      // make sure, that all ands from the input are in the cache for extraction
      const auto numLatches = spec->num_ands;
      aiger_and * const latchesBase = spec->ands;
      for(unsigned i = 0;i < numLatches; ++i){
        aiger_and* t_and = latchesBase + i;
        bddForAigerLit(t_and->lhs);
      }
    }

    for (auto[c_bdd, func_bdd] : func_by_var) {
      model_to_aiger(c_bdd, func_bdd);
    }
  }

  availableAigerNodes.clear();
  bddCache.clear();


//  if ( !Abc_NtkCheck( abcAigerDAG ) )
//  {
//    printf( "The AIG construction has failed.\n" );
//    return EXIT_FAILURE;
//  }

  // TODO run rewriting base on time remaining
  auto numberOfNodes = Abc_NtkNodeNum( abcAigerDAG );
  const auto originalNumberOfNodes = numberOfNodes;
//    if constexpr (ENABLE_LOGGING) {
//        const auto numOutputs = abc::Abc_NtkPoNum( abcAigerDAG);
//        for (int i = 0; i < numOutputs; ++i) {
//        auto output = Abc_NtkPo( abcAigerDAG, i );
//        std::string objName = Abc_ObjName(output);
//          auto root = Abc_ObjChild0(output);
//          global_logger->info("output: {0}, id: {1}, pointer: {2}, root: {3}",objName,output->Id, reinterpret_cast<uintptr_t>(output), reinterpret_cast<uintptr_t>(root));
//        }
//    }

  bool weCanRewrite = false;
  for (auto &&[output, lit] : outputs) {
    auto root = Abc_ObjChild0(output);
      const auto regularRoot = abc::Abc_ObjRegular(root);
      if(abc::Abc_AigNodeIsAnd(regularRoot)){
        // and -- check whether the node already has a lit set or whether we can use it directly
        auto iter = assignedLit.find(regularRoot);
        if (iter == assignedLit.end()) {
          weCanRewrite = true;
          break;
        }

    }
  }
  if(weCanRewrite){
    while (deadline > std::chrono::system_clock::now()){
      Abc_NtkRefactor( abcAigerDAG, 10, 16, 1, ("use zeros",1), ("useDcs",0), ("verbose",0) );
      if(deadline <= std::chrono::system_clock::now())
      {
        break;
      }
      Abc_NtkRewrite( abcAigerDAG, ("fUpdateLevel",0), ("fUseZeros",1), ("fVerbose",0), ("fVeryVerbose",0), ("fPlaceEnable",0) );
      auto newNumberOfNodes = Abc_NtkNodeNum( abcAigerDAG );
      if(newNumberOfNodes>=numberOfNodes){
        break;
      }
      numberOfNodes = newNumberOfNodes;
    }
  }
//  if constexpr (ENABLE_LOGGING) {
//    const auto numOutputs = abc::Abc_NtkPoNum( abcAigerDAG);
//    for (int i = 0; i < numOutputs; ++i) {
//      auto output = Abc_NtkPo( abcAigerDAG, i );
//      std::string objName = Abc_ObjName(output);
//      auto root = Abc_ObjChild0(output);
//      global_logger->info("output: {0}, id: {1}, pointer: {2}, root: {3}",objName,output->Id, reinterpret_cast<uintptr_t>(output), reinterpret_cast<uintptr_t>(root));
//    }
//  }

    if constexpr (ENABLE_LOGGING) {
        global_logger->info("number of nodes before: {0}, after: {1}",originalNumberOfNodes,Abc_NtkNodeNum( abcAigerDAG ));
    }
//  if constexpr (ENABLE_LOGGING) {
//    global_logger->info("createNeededAnds");
//  }
//  if ( !Abc_NtkCheck( abcAigerDAG ) )
//  {
//    printf( "The AIG construction has failed.\n" );
//    return EXIT_FAILURE;
//  }
  createNeededAnds();


  if (!outFile.empty()){
    auto res = aiger_open_and_write_to_file(spec, outFile.c_str());
    if constexpr (ENABLE_LOGGING) {
      if (!res) {
        global_logger->critical("Could not write the created aiger spec: {0}", outFile);
      }
    }
    auto file = fopen (outFile.c_str(), "a");
    printWinningRegion(winningRegion, spec, cudd, file);
  }
  else {
    auto res = aiger_write_to_file (spec, aiger_ascii_mode, stdout);
    if constexpr (ENABLE_LOGGING) {
      if (!res) {
        global_logger->error("Could not print the aiger spec");
      }
    }
    printWinningRegion(winningRegion, spec, cudd, stdout);
  }

  if constexpr (ENABLE_LOGGING) {
    global_logger->info("realizable");
    global_logger->info("new ands:{0}", spec->num_ands - numAnds);
  }

  return EXIT_STATUS_REALIZABLE;
}


void Synt::parse_into_spec(const char *aiger_file_name) {
  spec = aiger_init();
  auto res = aiger_open_and_read_from_file(spec, aiger_file_name);
  if(res){
    if constexpr (ENABLE_LOGGING) {
      global_logger->critical("Could not read the aiger spec: {0}", aiger_file_name);
    }
    std::exit(EXIT_FAILURE);
  }
}


void Synt::markInputsAndCategorizeControlledUncontrolledInputs() {
  const auto numInputs = spec->num_inputs;
  aiger_symbol * const inputsBase = spec->inputs;
  auto managerAdr = cudd.getManager();
  for(unsigned i = 0;i < numInputs; ++i){
    aiger_symbol* input = inputsBase + i;

    const auto lit = input->lit;
    Cudd_bddSetPiVar(managerAdr, lit);


    bddCache.emplace(lit, cudd.bddVar(lit));
    const auto pointer = cudd.bddVar(lit).getNode();

    string t_aig_name(trim(input->name));
    std::string prefix = "controllable";
    auto node = abc::Abc_NtkCreatePi( abcAigerDAG );
//    abc::Abc_ObjAssignName(node, const_cast<char *>(std::to_string(lit).c_str()), nullptr);
    if (prefix.size()<=t_aig_name.size() && std::equal(prefix.begin(), prefix.end(), t_aig_name.begin())) {
      controllableInputs.emplace_back(cudd.bddVar(lit));
      controllableInputLiterals.emplace_back(lit);
    }else{
      uncontrollableInputs.emplace_back(cudd.bddVar(lit));
    }
    availableAigerNodes.emplace(pointer,node);
    assignedLit.emplace(node,lit);
  }
}

void Synt::compose_init_state_bdd() {
  // Initial state is 'all latches are zero'
  init_state_bdd = cudd.bddOne();
  const auto numLatches = spec->num_latches;
  aiger_symbol * const latchesBase = spec->latches;
  auto managerAdr = cudd.getManager();
  std::vector<unsigned> latches{};
  for(unsigned i = 0;i < numLatches; ++i){
    aiger_symbol* t_latch = latchesBase + i;
    const auto lit = t_latch->lit;
    auto latchPresentStateVar = cudd.bddVar(lit);
    init_state_bdd &= !latchPresentStateVar;
    Cudd_bddSetPsVar(managerAdr,lit);

    // add it to the caches
    bddCache.emplace(lit,latchPresentStateVar);
    const auto pointer = latchPresentStateVar.getNode();


    auto node = abc::Abc_NtkCreatePi( abcAigerDAG );
//    abc::Abc_ObjAssignName(node, const_cast<char *>(std::to_string(lit).c_str()), nullptr);
    latches.emplace_back(t_latch->next);
    availableAigerNodes.emplace(pointer,node);
    assignedLit.emplace(node,lit);
  }

  for (const auto &latchNextId : latches) {
    auto pointer = bddForAigerLit(latchNextId).getNode();

      auto latchNew = abc::Abc_NtkCreatePo(abcAigerDAG);
//      abc::Abc_ObjAssignName( latchNew,  const_cast<char *>((std::string("ln")+std::to_string(latchNextId)).c_str()), nullptr);
      if(isComplemented(pointer)){
          abc::Abc_ObjAddFanin( latchNew, abc::Abc_ObjNot(availableAigerNodes.at(complement(pointer))));
      } else{
          abc::Abc_ObjAddFanin( latchNew, availableAigerNodes.at(pointer) );
      }

  }

}

void Synt::createTransitionCompositionVector() {
  // create the vector for composition : transitionComposition[current lit] = bdd(next state)

  const auto numberOfVars = Cudd_ReadSize(manager);
  transitionComposition.reserve((unsigned long) numberOfVars);

  // default projection
  for (int i = 0; i < numberOfVars; ++i) {
    transitionComposition.push_back(cudd.bddVar(i));
  }

  // set composition for latches
  const auto numLatches = spec->num_latches;
  if constexpr (ENABLE_LOGGING) {
    global_logger->info("compose transition bdd, nof_latches= {0}...", numLatches + 1);
  }
  for (unsigned i = 0; i < numLatches; ++i) {
    aiger_symbol *t_latch = spec->latches + i;
    transitionComposition[t_latch->lit] = bddForAigerLit(t_latch->next);
  }

  // create the array so that we do not have to create it on every call to VectorCompose. This means that we call the C interface.
  auto n = static_cast<size_t>(Cudd_ReadSize(manager));
  transitionCompositionArray = new DdNode *[n];
  for (size_t i = 0; i < n; i++) {
    transitionCompositionArray[i] = transitionComposition[i].getNode();
  }

}



BDD Synt::bddForAigerLit(unsigned lit) {
  unsigned stripped_lit = strip_lit(lit);

  if (is_negated(lit)) {
    if (!stripped_lit) {
      return cudd.bddOne();
    }
    if(aiger_is_input(spec, stripped_lit) != nullptr || aiger_is_latch(spec, stripped_lit)!= nullptr){
      return !cudd.bddVar(stripped_lit);
    }

  }else{
    if (!stripped_lit) {
      return cudd.bddZero();
    }
    if(aiger_is_input(spec, stripped_lit) != nullptr || aiger_is_latch(spec, stripped_lit)!= nullptr){
      return cudd.bddVar(stripped_lit);
    }
  }

  auto findResult = bddCache.find(stripped_lit);
  if(findResult != bddCache.end()){
    if (is_negated(lit)) {
      return !(findResult->second);
    }else{
      return findResult->second;
    }
  }
  // it must be an and because inputs and latches are already in the cache
  aiger_and* and_ = aiger_is_and(spec, stripped_lit);
  auto left = bddForAigerLit(and_->rhs0);
  auto right = bddForAigerLit(and_->rhs1);
  auto andBdd =  left & right;

  bddCache.emplace(stripped_lit,andBdd);

  // also construct the Aiger AND
  const auto leftPointer = left.getNode();
  bool negateLeft  = isComplemented(leftPointer);
  const auto rightPointer = right.getNode();
  bool negateRight = isComplemented(rightPointer);

  DdNode * normalizedLeftPointer = negateLeft ? complement(leftPointer) : leftPointer;
  DdNode * normalizedRightPointer = negateRight ? complement(rightPointer) : rightPointer;

  if ( !Cudd_IsConstant(andBdd.getNode())) {
    abc::Abc_Obj_t *leftNode =
        negateLeft ? abc::Abc_ObjNot(availableAigerNodes.at(normalizedLeftPointer)) : availableAigerNodes.at(
            normalizedLeftPointer);
    abc::Abc_Obj_t *rightNode =
        negateRight ? abc::Abc_ObjNot(availableAigerNodes.at(normalizedRightPointer)) : availableAigerNodes.at(
            normalizedRightPointer);

    if(abc::Abc_AigAndLookup(reinterpret_cast<abc::Abc_Aig_t *>(abcAigerDAG->pManFunc), leftNode, rightNode) == nullptr) {
      auto aigerNode = abc::Abc_AigAnd(reinterpret_cast<abc::Abc_Aig_t *>(abcAigerDAG->pManFunc), leftNode, rightNode);
      assignedLit.emplace(aigerNode,stripped_lit);
//      std::cout << "and "<< stripped_lit << "  " << reinterpret_cast<uintptr_t >(aigerNode) <<"\n";
      abc::Abc_NodeSetPersistant(aigerNode);


      auto pointer = andBdd.getNode();
      if (isComplemented(pointer)) {
        pointer = complement(pointer);
        availableAigerNodes.emplace(pointer, abc::Abc_ObjNot(aigerNode));
      } else {
        availableAigerNodes.emplace(pointer, aigerNode);
      }
    }
  }

  if (is_negated(lit)) {
    return !andBdd;
  }else{
    return andBdd;
  }
}

tuple<bool, vector<tuple<BDD, BDD>>, BDD> Synt::synthesize() {
//  """ Calculate winning region and extract output functions.
//
//  :return: - if realizable: <True, dictionary: controllable_variable_bdd -> func_bdd>
//      - if not: <False, None>
//  """
  BDD win_region = calc_win_region_functional();

  if (win_region == cudd.bddZero()){
    return {false, {},win_region};
  }

  if (checkRealizabilityOnly == SYNTHESIZE_TASK::REACHABILITY_ONLY) {
    return {true, {},win_region};
  }

  BDD primed_win_region_bdd(cudd,Cudd_bddVectorCompose(cudd.getManager(), win_region.getNode(), transitionCompositionArray));

  auto goodRegion = not_error_bdd & primed_win_region_bdd;

  std::vector<std::tuple<BDD, BDD>> func_by_var;

  if(strategyExtractionAlgorithm == STRATEGY_EXTRACTION::STANDARD) {
    func_by_var = extract_output_funcs_cpp(goodRegion);
  }
  if (strategyExtractionAlgorithm == STRATEGY_EXTRACTION::OPTIMIZED) {
    func_by_var = optimized_output(goodRegion);
  }
  if (strategyExtractionAlgorithm == STRATEGY_EXTRACTION::OPTIMIZED_BLOEM) {
    func_by_var = optimized_output_bloem(goodRegion);
  }
  return {true, func_by_var,win_region};
}

BDD Synt::calc_win_region_functional() {
//  """ Calculate a winning region for the safety game: win = greatest_fix_point.X [not_error & pre_sys(X)]
//  :return: BDD representing the winning region
//  """
  BDD fixpoint = cudd.bddOne();

  const BDD controllableCube = cudd.computeCube(controllableInputs);
  const BDD uncontrollableCube = cudd.computeCube(uncontrollableInputs);
  while(true){
    auto current = fixpoint;
    fixpoint =
        controlled_predecessor_system_functional(current, controllableCube, uncontrollableCube);
    if (fixpoint == current) {
      return fixpoint;
    }

    if(!(init_state_bdd <= fixpoint)){
      return cudd.bddZero();
    }

  }
}



BDD Synt::controlled_predecessor_system_functional(const BDD &a_region,
                                                   const BDD &controllableCube,
                                                   const BDD &uncontrollableCube) {
  BDD next_state(cudd,Cudd_bddVectorCompose(manager, a_region.getNode(), transitionCompositionArray));
  return next_state.AndAbstract(not_error_bdd,controllableCube).UnivAbstract(
      uncontrollableCube);
}

vector<std::tuple<BDD, BDD>> Synt::extract_output_funcs_cpp(BDD non_det_strategy) {
//  """
//  Calculate BDDs for output functions given a non-deterministic winning strategy.
//      Cofactor-based approach.
//
//  :return: dictionary ``controllable_variable_bdd -> func_bdd``
//  """
  vector<std::tuple<BDD, BDD>>  output_models;
  if(controllableInputLiterals.empty()){
    return output_models;
  }
  output_models.reserve(controllableInputs.size());
  const auto numControllables = controllableInputLiterals.size();

  vector<BDD> others = controllableInputs;
  others.erase(begin(others));
  const auto multipleControllableInputs = !others.empty();

  for (auto i = decltype(numControllables){0}; i < numControllables; ++i) {
    auto& controllable = controllableInputs[i];

    BDD c_arena;
    if(multipleControllableInputs){
      BDD others_cube = cudd.computeCube(others);
      c_arena = non_det_strategy.ExistAbstract(others_cube);
    } else {
      c_arena = non_det_strategy;
    }

    BDD c_model = c_arena.Cofactor(controllable); // states (x,i) in which controllable can be true

    output_models.emplace_back(controllable,c_model);

    non_det_strategy = non_det_strategy.Compose(c_model,controllable.NodeReadIndex());
    if(multipleControllableInputs){
      others[i] = controllable;  // add the current one and removes the next one
    }
  }

  return output_models;
}

vector<std::tuple<BDD,BDD>> Synt::optimized_output(const BDD &a_strategy) {

  vector<std::tuple<BDD,BDD>> optimized_strat;
  if(controllableInputLiterals.empty()){
    return optimized_strat;
  }
  auto strat = a_strategy;
  const auto numControllables = controllableInputLiterals.size();

  vector<BDD> others = controllableInputs;
  others.erase(begin(others));

  const auto multipleControllableInputs = !others.empty();
  for (auto i = decltype(numControllables){0}; i < numControllables; ++i) {
    auto var = controllableInputLiterals[i];
    auto& varLit = controllableInputs[i];

    BDD strat_xc;
    if(multipleControllableInputs){
      strat_xc = strat.ExistAbstract(cudd.computeCube(others));
    } else {
      strat_xc = strat;
    }

    auto maybe_true = strat_xc.Cofactor(varLit); // p
    auto maybe_false = strat_xc.Cofactor(!varLit); // n


    auto mustbe_true = (!maybe_false) & maybe_true;
    auto mustbe_false = (!maybe_true) & maybe_false;

    auto care_set = mustbe_true | mustbe_false;
    auto f_xcT = mustbe_true.Restrict(care_set);
    auto f_xcF = (!mustbe_false).Restrict(care_set);

    if(f_xcF.nodeCount() < f_xcT.nodeCount()){
      optimized_strat.emplace_back(varLit,f_xcF);
      strat = strat.Compose(f_xcF,var);
    }else{
      optimized_strat.emplace_back(varLit,f_xcT);
      strat = strat.Compose(f_xcT,var);
    }

    if(multipleControllableInputs){
      others[i] = varLit; // add the current one and removes the next one
    }
  }

  return optimized_strat;
}

vector<std::tuple<BDD,BDD>> Synt::optimized_output_bloem(const BDD &a_strategy) {
  vector<std::tuple<BDD,BDD>> optimized_strat;
  if(controllableInputLiterals.empty()){
    return optimized_strat;
  }
  const auto numControllables = controllableInputLiterals.size();
  auto strat = a_strategy;

  vector<BDD> others = controllableInputs;
  others.erase(begin(others));

  const auto multipleControllableInputs = !others.empty();

  for (auto i = decltype(numControllables){0}; i < numControllables; ++i) {
    auto var = controllableInputLiterals[i];
    auto& varLit = controllableInputs[i];

    BDD strat_xc;
    if(multipleControllableInputs){
      strat_xc = strat.ExistAbstract(cudd.computeCube(others));
    } else {
      strat_xc = strat;
    }

    auto p_cofactor = strat_xc.Cofactor(varLit); // p
    auto n_cofactor = strat_xc.Cofactor(!varLit); // n

    auto t_p_cofactor = p_cofactor & !n_cofactor;
    auto t_n_cofactor = n_cofactor & !p_cofactor;

    p_cofactor = t_p_cofactor;
    n_cofactor = t_n_cofactor;

    for(auto& a_var : uncontrollableInputs) {
      t_p_cofactor = p_cofactor.ExistAbstract(a_var);
      t_n_cofactor = n_cofactor.ExistAbstract(a_var);

      if((t_p_cofactor & t_n_cofactor) == cudd.bddZero()) {
        p_cofactor = t_p_cofactor;
        n_cofactor = t_n_cofactor;
      }
    }

    auto care_set = ((!n_cofactor) & p_cofactor) | ((!p_cofactor) & n_cofactor);
    auto f_xc = p_cofactor.Restrict(care_set);

    optimized_strat.emplace_back(varLit,f_xc);
    strat = strat.Compose(f_xc,var);
    if(multipleControllableInputs){
      others[i] = varLit;  // add the current one and removes the next one
    }
  }

  return optimized_strat;
}


abc::Abc_Obj_t * Synt::walk(const BDD &a_bdd) {
//  """
//  Walk given BDD node (recursively).
//  If given input BDD requires intermediate AND gates for its representation, the function adds them.
//      Literal representing given input BDD is `not` added to the spec.
//
//  :returns: literal representing input BDD
//  :warning: variables in cudd nodes may be complemented, check with: ``node.IsComplement()``
//  """

  if ( Cudd_IsConstant(a_bdd.getNode())) {
    if(a_bdd == cudd.bddOne()){
      return abc::Abc_AigConst1(abcAigerDAG);
    }
    else
    {
      return abc::Abc_ObjNot(abc::Abc_AigConst1(abcAigerDAG));
    }
  }

  auto a_lit = a_bdd.NodeReadIndex();

  const auto pointer = a_bdd.getNode();
  const auto isComplementedPointer = isComplemented(pointer);
  auto negatedBdd = !a_bdd;
  auto complementedPointer = negatedBdd.getNode();
  if(isComplementedPointer){
    auto result = availableAigerNodes.find(complementedPointer);
    if(result != availableAigerNodes.end()){
        return abc::Abc_ObjNot(result->second);
    }
    return abc::Abc_ObjNot(walk(!a_bdd));
  } else{
    auto result = availableAigerNodes.find(pointer);
    if(result != availableAigerNodes.end()){
      return result->second;
    }
  }
//  if constexpr (ENABLE_LOGGING) {
//    global_logger->info("walk bdd: {0}", reinterpret_cast<uintptr_t >(pointer));
//  }

  // we made sure that the bdd is not complemented

  // a_bdd = ite(a, then_bdd, else_bdd)
  // = a*then + !a*else
  // = !(!(a*then) * !(!a*else))
  // -> in general case we need 3 more ANDs

  BDD a = cudd.bddVar(a_lit);

  BDD thenBdd = BDD(cudd,Cudd_T(a_bdd.getNode()));
  BDD elseBdd = BDD(cudd,Cudd_E(a_bdd.getNode()));

  BDD notAAndThen = !(a & thenBdd);
  auto decider = availableAigerNodes.at(a.getNode());
  auto notAAndThenNode = getOrCreateNand(notAAndThen, decider,thenBdd);
  BDD notAAndElse = !((!a) & elseBdd);
  auto notAAndElseNode = getOrCreateNand(notAAndElse, abc::Abc_ObjNot(decider),elseBdd);

  auto newAndNode = abc::Abc_AigAnd(reinterpret_cast<abc::Abc_Aig_t *>(abcAigerDAG->pManFunc), notAAndThenNode, notAAndElseNode );
  addedAnds.push_back(a_bdd);
  auto nodePointerResult = abc::Abc_ObjNot(newAndNode);
  availableAigerNodes.emplace(pointer, nodePointerResult);
  return nodePointerResult;
}

abc::Abc_Obj_t * Synt::getOrCreateNand(BDD &nand, abc::Abc_Obj_t *deciderNode, BDD &branchBdd) {
  if ( Cudd_IsConstant(nand.getNode())) {
    if(nand == cudd.bddOne()){
      return abc::Abc_AigConst1(abcAigerDAG);
    }
    else
    {
      return abc::Abc_ObjNot(abc::Abc_AigConst1(abcAigerDAG));
    }
  }

  const auto pointer = nand.getNode();
  const auto isComplementedPointer = isComplemented(pointer);
  auto negatedBdd = !nand;
  auto complementedPointer = negatedBdd.getNode();
  if(isComplementedPointer){
    auto result = availableAigerNodes.find(complementedPointer);
    if(result != availableAigerNodes.end()){
      return abc::Abc_ObjNot(result->second);
    }
  } else{
    auto result = availableAigerNodes.find(pointer);
    if(result != availableAigerNodes.end()){
      return result->second;
    }
  }

  // the cache doesn't have it, so we create it
  auto branchLit = walk(branchBdd);
  auto newAndNode = abc::Abc_AigAnd(reinterpret_cast<abc::Abc_Aig_t *>(abcAigerDAG->pManFunc), deciderNode, branchLit );
  auto newNandNode= abc::Abc_ObjNot(newAndNode);
  if(isComplementedPointer){
    addedAnds.push_back(nand);
    availableAigerNodes.emplace(complementedPointer,newAndNode);
    return newNandNode;
  }else{
    addedAnds.push_back(nand);
    availableAigerNodes.emplace(pointer,newNandNode);
    return newNandNode;
  }
}

void Synt::model_to_aiger(const BDD &c_bdd, const BDD &func_bdd) {
  const std::int32_t c_lit = c_bdd.NodeReadIndex();
//  if constexpr (ENABLE_LOGGING) {
//    global_logger->info("model_to_aiger: {0}",c_lit);
//  }
  abc::Abc_Obj_t * func_as_aiger_node = walk(func_bdd);
  auto aigerNode = abc::Abc_AigAnd(reinterpret_cast<abc::Abc_Aig_t *>(abcAigerDAG->pManFunc), func_as_aiger_node, func_as_aiger_node );
  const auto oldNode = availableAigerNodes.at(c_bdd.getNode());

//#ifndef NDEBUG
//  std::cout <<"\n\n Old node:"<< std::endl;
//  abc::Abc_AigPrintNode(oldNode);
////  printNet(oldNode);
//  std::cout << "fanout old " << abc::Abc_ObjFanoutNum(oldNode) << std::endl;
//  std::cout << "\n New node:"<< std::endl;
//  if(abc::Abc_ObjIsComplement(aigerNode)){
//    std::cout << "complemented"<< std::endl;
//  }
//  abc::Abc_AigPrintNode(aigerNode);
////  printNet(aigerNode);
//  std::cout << "fanout new "<<abc::Abc_ObjFanoutNum(abc::Abc_ObjRegular(aigerNode)) << std::endl;
//
//#endif

  abc::Abc_Obj_t * aigerRegular = abc::Abc_ObjRegular(aigerNode);

  abc::Abc_Obj_t * output = nullptr;
  if(abc::Abc_AigNodeIsAnd(aigerRegular)){
    output = abc::Abc_NtkCreatePo(abcAigerDAG);
    outputs.emplace_back(output,c_lit);
    abc::Abc_ObjAddFanin( output, oldNode );
    if(Abc_ObjFaninC0( output )){
      std::cout << "output fanin already complemented"<< std::endl;
    }
  } else{
    aiger_remove_input(spec, (unsigned int) c_lit);
    if(abc::Abc_AigNodeIsConst(aigerNode)){
      if(abc::Abc_ObjIsComplement(aigerNode)){
        aiger_add_and(spec, (unsigned int) c_lit, 0, 0);
      }else{
        aiger_add_and(spec, (unsigned int) c_lit, 1, 1);
      }
    }else{
      // latch or PI
      unsigned int lit = assignedLit.at(aigerRegular);
      if(abc::Abc_ObjIsComplement(aigerNode)){
        negate(lit);
      }
      aiger_add_and(spec, (unsigned int) c_lit, lit, lit);
    }
  }
  abc::Abc_AigReplace2(reinterpret_cast<abc::Abc_Aig_t *>(abcAigerDAG->pManFunc), oldNode, aigerNode, 1);
//#ifndef NDEBUG
//  if(output){
//    std::cout << "\n Node after replacement:"<< std::endl;
//    auto root = Abc_ObjChild0(output);
//    if(abc::Abc_ObjIsComplement(root)){
//      std::cout << "complemented"<< std::endl;
//    }
////    printNet(root);
//    abc::Abc_AigPrintNode(root);
//    std::cout << "fanout replacement "<<abc::Abc_ObjFanoutNum(abc::Abc_ObjRegular(root)) << std::endl;
//  }
//#endif
}

void Synt::printNet(abc::Abc_Obj_t *aigerNode) {
  abc::Abc_AigPrintNode(aigerNode);
  const auto regular = abc::Abc_ObjRegular(aigerNode);
  abc::Abc_ObjPrint( stdout, regular );
  if(abc::Abc_AigNodeIsAnd(regular)){
    printNet(Abc_ObjChild0(regular));
    printNet(Abc_ObjChild1(regular));
  }
}

void Synt::createNeededAnds() {
  // we skip the first output because it is the error_output
  for (auto &&[output, lit] : outputs) {
    auto root = Abc_ObjChild0(output);
    if(abc::Abc_AigNodeIsConst(root)){
      // const -- we have to add an and
      if(abc::Abc_ObjIsComplement(root)){
        aiger_remove_input(spec, lit);
        aiger_add_and(spec, lit, 0, 0);
      }else{
        aiger_remove_input(spec, lit);
        aiger_add_and(spec, lit, 1, 1);
      }
    }else{
      const auto regularRoot = abc::Abc_ObjRegular(root);
      if(abc::Abc_AigNodeIsAnd(regularRoot)){
        // and -- check whether the node already has a lit set or whether we can use it directly
        auto iter = assignedLit.find(regularRoot);
        if (iter != assignedLit.end()) {
          // it already has a lit in the output so we have to add another
          unsigned int andLit = iter->second;
          if(abc::Abc_ObjIsComplement(root)){
            andLit = negate(andLit);
          }
          aiger_remove_input(spec, lit);
          aiger_add_and(spec, lit, andLit, andLit);
        }else {

          // we can simply use this one
          std::vector<abc::Abc_Obj_t *> dfsStack{abc::Abc_ObjRegular(Abc_ObjFanin0(regularRoot)), abc::Abc_ObjRegular(Abc_ObjFanin1(regularRoot))};
          while(!dfsStack.empty()){
            auto currentNode = dfsStack.back();
            if(!static_cast<bool>(abc::Abc_AigNodeIsAnd(currentNode))){
              dfsStack.pop_back();
              continue;
            }
            if(Abc_NodeIsPersistant(currentNode)){
              dfsStack.pop_back();
              continue;
            }
            // we have a non persistent and
            auto localIter = assignedLit.find(currentNode);
            if (localIter != end(assignedLit)) {
              // we already have assigned a lit
              dfsStack.pop_back();
              continue;
            }

            auto leftNode = Abc_ObjChild0( currentNode ) ;
            auto rightNode = Abc_ObjChild1( currentNode );

            int leftLit=tryToGetLit(leftNode);
            int rightLit=tryToGetLit(rightNode);
            if(leftLit==-1){
              dfsStack.push_back(abc::Abc_ObjRegular(Abc_ObjFanin0(currentNode)));
            }
            if(rightLit==-1){
              dfsStack.push_back(abc::Abc_ObjRegular(Abc_ObjFanin1(currentNode)));
            }
            if(leftLit!=-1 && rightLit!=-1){
              dfsStack.pop_back();
              auto newLit = (spec->maxvar+1)*2;
              abc::Abc_NodeSetPersistant(currentNode);
              assignedLit.emplace(currentNode,newLit);
              aiger_add_and(spec, newLit,
                            static_cast<unsigned int>(leftLit),
                            static_cast<unsigned int>(rightLit));
            }
          }


          auto leftNode = Abc_ObjChild0(regularRoot) ;
          auto rightNode = Abc_ObjChild1(regularRoot);
          assignedLit.emplace(regularRoot,lit);
          abc::Abc_NodeSetPersistant(regularRoot);
          if(Abc_ObjIsComplement(root)){
            // we have to introduce another node (only in the output)
            auto newLit = (spec->maxvar+1)*2;
            aiger_add_and(spec, newLit,
                          static_cast<unsigned int>(tryToGetLit(leftNode)),
                          static_cast<unsigned int>(tryToGetLit(rightNode)));
            aiger_remove_input(spec, lit);
            aiger_add_and(spec, lit,
                          negate(newLit),
                          negate(newLit));
          }else
          {
            aiger_remove_input(spec, lit);
            aiger_add_and(spec, lit,
                          static_cast<unsigned int>(tryToGetLit(leftNode)),
                          static_cast<unsigned int>(tryToGetLit(rightNode)));
          }

        }
      }else{
        // latch out or PI -- we have to add an and
        unsigned int piLit = assignedLit.at(regularRoot);
        if(abc::Abc_ObjIsComplement(root)){
          piLit = negate(piLit);
        }
        aiger_remove_input(spec, lit);
        aiger_add_and(spec, lit, piLit, piLit);
      }
    }
  }
}
int Synt::tryToGetLit(abc::Abc_Obj_t *node) {
  int lit = -1;
  auto nodeIsNegated = static_cast<bool>(Abc_ObjIsComplement(node));
  const auto regularNode = abc::Abc_ObjRegular(node);

  auto iter = assignedLit.find(regularNode);

  if(iter != end(assignedLit)){
    lit = nodeIsNegated ? negate(iter->second) : iter->second;
  }

  return lit;
}


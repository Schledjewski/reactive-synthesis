
#ifndef REACTIVE_SYNTHESIS_PORT_SYNT_H_H
#define REACTIVE_SYNTHESIS_PORT_SYNT_H_H

#include <cstdlib> // wird anscheinend fuer cudd benoetigt
#include <iostream>
#include <vector>
#include <cassert>
#include <tuple>
#include <string>
#include <algorithm>
#include <map>
#include <random>
#include <chrono>

#include <base/abc/abc.h>

#include <misc/util/abc_global.h>
extern "C" {
#include "cudd.h"
#include "aiger.h"
void aiger_redefine_input_as_and (aiger* pub, unsigned input_lit, unsigned rhs0, unsigned rhs1);
void aiger_remove_input(aiger* pub, unsigned input_lit);
}
#include "cuddObj.hh"

#include "spdlog/spdlog.h"
namespace spd = spdlog;

#ifdef NDEBUG
  constexpr bool ENABLE_LOGGING = false;
#else
  constexpr bool ENABLE_LOGGING = true;
#endif

constexpr int EXIT_STATUS_REALIZABLE = 10;
constexpr int EXIT_STATUS_UNREALIZABLE = 20;
enum class DYNAMIC_REORDERING {
  CUDD_REORDER_SIFT,
  CUDD_REORDER_SIFT_CONVERGE,
  CUDD_REORDER_SYMM_SIFT,
  CUDD_REORDER_SYMM_SIFT_CONV,
  CUDD_REORDER_GROUP_SIFT,
  CUDD_REORDER_GROUP_SIFT_CONV,
  CUDD_REORDER_LAZY_SIFT};
enum class STRATEGY_EXTRACTION {STANDARD, OPTIMIZED, OPTIMIZED_BLOEM};
enum class SYNTHESIZE_TASK {REACHABILITY_ONLY, SYNTHESIZE};
enum class OUTPUT_AIGER {ADD_OUTPUT, NO_NEW_OUTPUT};


class Synt{
public:
  explicit Synt(DYNAMIC_REORDERING dynamicReordering,
                STRATEGY_EXTRACTION strategyExtractionAlgorithm,
                SYNTHESIZE_TASK realizabilityOnly,
                OUTPUT_AIGER introduce_output,
                const std::string &aigerFile,
                const std::string &outFile,
                std::chrono::system_clock::time_point &deadline);

  int run();


  ~Synt();

private:
  Cudd cudd{};
  aiger* spec = nullptr;
  std::shared_ptr<spd::logger> global_logger;
  //AigerDAG aigerDAG; TODO
  abc::Abc_Ntk_t * abcAigerDAG;

  DYNAMIC_REORDERING dynamicReordering;
  STRATEGY_EXTRACTION strategyExtractionAlgorithm;
  SYNTHESIZE_TASK checkRealizabilityOnly;
  OUTPUT_AIGER introduce_output;

  const std::string outFile;

  BDD init_state_bdd;
  std::vector<unsigned int> controllableInputLiterals;
  std::vector<BDD> controllableInputs;
  std::vector<BDD> uncontrollableInputs;

  // used for walk: non-complemented BDD pointer -> aiger pointer --- after the walk we can clear this because during rewriting the pointers might change
  // std::unordered_map< DdNode*, TaggedAigerNodePointer> availableAigerNodes{}; TODO
  std::unordered_map< DdNode*, abc::Abc_Obj_t *> availableAigerNodes{};
  // cache for construction: non-negated literal -> BDD
  std::unordered_map<unsigned int,BDD> bddCache;

  BDD not_error_bdd;
  std::vector<BDD> transitionComposition;
  DdNode **transitionCompositionArray = nullptr;
  DdManager* manager = nullptr;

  std::vector<BDD> addedAnds; // we must keep a ref to them, so that the pointer in availableAndsInputsLatches stays valid

  std::vector<std::tuple<abc::Abc_Obj_t *, unsigned int>> outputs{};
  std::unordered_map<abc::Abc_Obj_t *, unsigned int> assignedLit{};

  void parse_into_spec(const char* aiger_file_name);
  aiger_symbol* get_err_symbol();

  void markInputsAndCategorizeControlledUncontrolledInputs();
  void compose_init_state_bdd();
  void createTransitionCompositionVector();

  BDD bddForAigerLit(unsigned lit);

  std::tuple<bool, std::vector<std::tuple<BDD, BDD>>, BDD> synthesize();
  BDD controlled_predecessor_system_functional(const BDD &a_region,
                                                 const BDD &controllableCube,
                                                 const BDD &uncontrollableCube);

  BDD calc_win_region_functional();

  std::vector<std::tuple<BDD, BDD>> extract_output_funcs_cpp(BDD non_det_strategy);
  std::vector<std::tuple<BDD,BDD>> optimized_output(const BDD& a_strategy);
  std::vector<std::tuple<BDD,BDD>> optimized_output_bloem(const BDD &a_strategy);

  abc::Abc_Obj_t * walk(const BDD &a_bdd);
  abc::Abc_Obj_t * getOrCreateNand(BDD &nand, abc::Abc_Obj_t *deciderNode, BDD &branchBdd);
  void model_to_aiger(const BDD &c_bdd, const BDD &func_bdd);

  void printNet(abc::Abc_Obj_t * pNode);
  int tryToGetLit(abc::Abc_Obj_t * pNode);
  void createNeededAnds();

  std::chrono::system_clock::time_point deadline;
};

#endif //REACTIVE_SYNTHESIS_PORT_SYNT_H_H

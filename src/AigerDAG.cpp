//
// Created by mschledjewski on 5/23/18.
//

#include "AigerDAG.h"
#include <algorithm>

AigerNode::AigerNode(TaggedAigerNodePointer left,
                     TaggedAigerNodePointer right,
                     BloombergLP::bdlma::MultipoolAllocator &vectorAllocator,
                     int32_t aigerId) : left(left), right(right), parents(&vectorAllocator), cuts(&vectorAllocator), aigerId(aigerId)
{


}


struct FourCutComparison{
  bool operator()(const FourCut &a, const FourCut &b){
    const auto sizeA = a.size();
    const auto sizeB = b.size();
    if(sizeA > sizeB){
      return true; // larger ones come first
    }

    if(sizeA < sizeB){
      return false; // larger ones come first
    }

    for (int i = 0; i < sizeA; ++i) {
      auto aValue = reinterpret_cast<uintptr_t>(a[i]);
      auto bValue = reinterpret_cast<uintptr_t>(b[i]);
      if(aValue != bValue){
        return aValue < bValue; // sort them  lexicographically in descending order
      }
    }
    return false;
  };
};



struct FourCutPointerComparison{
  bool operator()(const AigerNode* a, const AigerNode* b){
    auto aValue = reinterpret_cast<uintptr_t>(a);
    auto bValue = reinterpret_cast<uintptr_t>(b);
    return aValue < bValue;
  };
};


AigerDAG::AigerDAG() :
    inputs2Node(),
    vectorAllocator(),
    nodeAllocator(100,BloombergLP::bsls::BlockGrowth::Strategy::BSLS_CONSTANT),
    roots(&vectorAllocator)
    {
  auto LOGIC_ZEROptr = new (nodeAllocator) AigerNode(TaggedAigerNodePointer(nullptr), TaggedAigerNodePointer(nullptr),vectorAllocator,0);
  LOGIC_ZEROptr->refCount = 1;
  LOGIC_ZEROptr->isInOriginal=true;
  LOGIC_ZERO = TaggedAigerNodePointer{LOGIC_ZEROptr};
}


TaggedAigerNodePointer AigerDAG::addVariableNode(unsigned int varId, bool isControllableInput) {
  if(isControllableInput){
    AigerNode *ptr = new (nodeAllocator) AigerNode(TaggedAigerNodePointer(nullptr), TaggedAigerNodePointer(reinterpret_cast<AigerNode*>(varId)),vectorAllocator,varId);
    ptr->isInOriginal = true;
    ptr->cuts.emplace_back(FourCut{ptr}); // trivial cut only
    ptr->refCount = 1; // this ensures that the count will always be > 0.

    roots.emplace_back(varId,ptr);
    return TaggedAigerNodePointer(ptr);
  }


  AigerNode *ptr = new (nodeAllocator) AigerNode(TaggedAigerNodePointer(nullptr), TaggedAigerNodePointer(nullptr),vectorAllocator,varId);
  ptr->isInOriginal = true;
  ptr->cuts.emplace_back(FourCut{ptr}); // trivial cut only
  ptr->refCount = 1; // this ensures that the count will always be > 0.
  // We DO NOT have to insert it into the map, because we will never look for it.
  // This is because it is never between the 4 input nodes and the node currently being rewritten.
  return TaggedAigerNodePointer(ptr);
}


TaggedAigerNodePointer AigerDAG::addOriginalAnd(TaggedAigerNodePointer left, TaggedAigerNodePointer right, unsigned int lit) {
  // we enforce left input <= right input to make share as much as possible
  auto key = left <= right ? AigerNodeKey{left,right} : AigerNodeKey{right,left};
  auto entry = inputs2Node.find(key);
  if(entry != inputs2Node.end()){
    return  TaggedAigerNodePointer(entry->second); // we already have another original one in the
  }

  AigerNode *ptr = new (nodeAllocator) AigerNode(left, right, vectorAllocator, lit);
  ptr->isInOriginal = true;
  ptr->refCount = 1; // this ensures that the count will always be > 0.
  mergeChildrenCuts(*ptr);
  left->refCount += 1;
  right->refCount +=1;
  inputs2Node.insert({key,ptr});

  return TaggedAigerNodePointer(ptr);
}


/**
 * Adds a new AND node.
 *
 * We assume that the node is not already in the graph.
 * During initial construction, this is guaranteed by checking by the associated BDD.
 * @param left
 * @param right
 * @return
 */
TaggedAigerNodePointer AigerDAG::addAnd(TaggedAigerNodePointer left, TaggedAigerNodePointer right) {
  // we enforce left input <= right input to make share as much as possible
  auto key = left <= right ? AigerNodeKey{left,right} : AigerNodeKey{right,left};
  auto entry = inputs2Node.find(key);
  if(entry != inputs2Node.end()){
    return  TaggedAigerNodePointer(entry->second); // we already have another original one in the
  }

  AigerNode *ptr = new (nodeAllocator) AigerNode(left, right, vectorAllocator);
  mergeChildrenCuts(*ptr);
  left->refCount += 1;
  right->refCount +=1;
  inputs2Node.insert({key,ptr});

  return TaggedAigerNodePointer(ptr);
}


/**
 * Compute the 4-cuts based on the children's 4-cuts
 * @param node
 * @return Is true, if 4-cuts changed. This means we have to propagate to all parents.
 */
bool AigerDAG::mergeChildrenCuts(AigerNode &node) {
  bsl::vector<FourCut> cuts{&vectorAllocator};
  // keep larger cuts at the beginning so that when using the cuts we can simply stop once cut.size() < 4
  for (auto &&leftCut : node.left->cuts) {
    for (auto &&rightCut : node.right->cuts) {
      if(leftCut.size()==4 && rightCut.size()==4){
        if(leftCut == rightCut){
          cuts.emplace_back(leftCut);
        }
      }
      else
      {
        FourCut newCut = leftCut;
        // combine the vectors, sort it and remove duplicates
        newCut.insert(newCut.end(),rightCut.begin(),rightCut.end());
        std::sort(newCut.begin(),newCut.end(),FourCutPointerComparison{});
        auto newEnd = std::unique(newCut.begin(),newCut.end());
        newCut.erase(newEnd,newCut.end());

        if(newCut.size() <=4 && std::find(cuts.begin(),cuts.end(),newCut) == cuts.end())
        {
          cuts.push_back(newCut);
        }
      }
    }
  }
  cuts.emplace_back(FourCut{&node}); // add the trivial cut
  std::sort(cuts.begin(),cuts.end(),FourCutComparison{}); // sort the vector
  if(cuts != node.cuts)
  {
    cuts.swap(node.cuts);
    return true;
  }
  return false;
}



void AigerDAG::extractNewAnds(aiger *spec) {
  // try to optimize the root ands away
  // we can do this if the child node is not an original node and it is not already claimes by one of the others
  // we can therefore simply check whether the child is not an original node and then remove it
  // another root that also uses this output will then see the node marked as original.

  for (auto &&[id, aigerNode] : roots) {
    if(!aigerNode->left->isInOriginal){
      optimizeChildAway(aigerNode);
    }
  }

  // now we mark the roots as not in the original
  // therefore we can then do a walk and simply output all non-original nodes
  for (auto &[id, aigerNode] : roots) {
    aigerNode->isInOriginal = false;
  }

  ++currentDfsTraversalId;

  bsl::vector< AigerNode*> dfsStack{&vectorAllocator};
  // push all roots onto the DFS stack
  for (const auto &[id, node] : roots) {
    dfsStack.push_back(node);
  }

  // do the dfs
  while(!dfsStack.empty()){
    AigerNode* currentNode = dfsStack.back();

    if(currentNode->isInOriginal){
      dfsStack.pop_back();
      continue; // we do not have to output nodes that are in the original
    }

    if(currentNode->dfsTraversalId != currentDfsTraversalId){
      // first time that we visit it, therefore add the children to the stack
      dfsStack.push_back(currentNode->left.getUnmodifiedPointer());
      dfsStack.push_back(currentNode->right.getUnmodifiedPointer());
      currentNode->dfsTraversalId = currentDfsTraversalId;
    }
    else
    {
      // second time that we visit
      // we can therefore output this node
      dfsStack.pop_back();

      //set the aiger id based on counter if not set
      if(currentNode->aigerId == -1){
        currentNode->aigerId = 2*(spec->maxvar+1);
      }

      aiger_add_and(spec, static_cast<unsigned int>(currentNode->aigerId),
                    static_cast<unsigned int>(currentNode->left->aigerId),
                    static_cast<unsigned int>(currentNode->right->aigerId));
    }
  }

}


void replaceChildEdge(const TaggedAigerNodePointer& newChild, const TaggedAigerNodePointer& negatedNewChild, const TaggedAigerNodePointer& oldChild, const TaggedAigerNodePointer&negatedOldChild, AigerNode* parent)
{
  if(parent->left == oldChild){
    parent->left = newChild;
  }else
  {
    if(parent->left == negatedOldChild){
      parent->left = negatedNewChild;
    }
  }
  if(parent->right == oldChild){
    parent->right = newChild;
  }else
  {
    if(parent->right == negatedOldChild){
      parent->right = negatedNewChild;
    }
  }
}

/**
 * @param node
 *      The node whose child should be optimized away.
 *
 * We do not need to update cuts or so, because we are already creating the output.
 * This also means that we do not need to change map.
 */
void AigerDAG::optimizeChildAway(AigerNode * node) {
  auto child = node->left;
  // copy the children
  node->left  = child->left;
  node->right = child->right;

  // update all parents to reference this node
  {
    TaggedAigerNodePointer negatedChild(child,true);
    TaggedAigerNodePointer newChild(node);
    TaggedAigerNodePointer negatedNewChild(node,true);
    for (const auto &parent : node->parents) {
      replaceChildEdge(newChild,negatedNewChild,child,negatedChild,parent);
    }
  }

}
void AigerDAG::setControllableInputFunction(std::int32_t lit, TaggedAigerNodePointer func_as_aiger_node) {
  for (const auto &[id, node] : roots) {
    if(id == lit){
      node->left = func_as_aiger_node;
      node->right = func_as_aiger_node;
      func_as_aiger_node->refCount += 1;
      // We do not have to add this node to the map because we will never look for it.
      // Otherwise we would have problems if two controllable inputs use the same strategy.
      return;
    }
  }
}






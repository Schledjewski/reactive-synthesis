# How to build

We use cmake. 

We use `-march=native` so if you build on a system that differs from the benchmark system, 
please change this in CMakeLists.txt at line 24 and 29.

So you probably use something like:

```shell
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=gcc-7 -DCMAKE_CXX_COMPILER=g++-7 -DBUILD_SHARED_LIBS=OFF ..
make
```

The program should be called as:

```shell
synt ${inputfile_name}
```
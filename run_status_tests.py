#!/usr/bin/env python3.6
import os
from subprocess import run,PIPE

############################################################################
# change these two paths according to your setup

# should be executable, to make it executable run: chmod +x ./synt.py
tool = "../build/synt"

tests_dir = "./testcases"

############################################################################
EXIT_STATUS_REALIZABLE = 10
EXIT_STATUS_UNREALIZABLE = 20


tests = sorted([(os.path.abspath(tests_dir + '/' + f),f)
                for f in os.listdir(tests_dir) if f.endswith('.aag')])


def cat(test):
    f = open(test)
    res = f.readlines()
    f.close()
    return res


def is_realizable(test):
    spec_status = cat(test)[-1].strip()
    if spec_status == 'realizable':
        return True
    if spec_status == 'unrealizable':
        return False
    assert 0, 'spec status is unknown'


if __name__ == "__main__":
    allPassed = True
    Ds = [['-d CUDD_REORDER_SIFT'],['-d CUDD_REORDER_SIFT_CONVERGE'],['-d CUDD_REORDER_SYMM_SIFT'],['-d CUDD_REORDER_SYMM_SIFT_CONV'],['-d CUDD_REORDER_GROUP_SIFT'],['-d CUDD_REORDER_GROUP_SIFT_CONV'],['-d CUDD_REORDER_LAZY_SIFT']]
    Es = [[],['-e optimized'],['-e optimized_cofactor']]
    Ds = [['-d CUDD_REORDER_LAZY_SIFT']]
    Es = [['-e optimized_cofactor']]
    for e in Es:
        for d in Ds:
            print("============== ")
            for (t,filename) in tests:
                command = [tool,t]+d+e+["-o",(filename+"test.aag")]
                print ('running ' + " ".join(command))
                runres = run(command ,cwd='./testoutput', stdout=PIPE)
                #print(runres.stdout)
                res =runres.returncode
                assert res in [EXIT_STATUS_REALIZABLE, EXIT_STATUS_UNREALIZABLE], 'unknown status: ' + str(res)

                if res == EXIT_STATUS_REALIZABLE and not is_realizable(t):
                    print ()
                    print (t)
                    print ('FAILED: should be unrealizable: tool found it realizable. hm.'.format(test=t))
                    allPassed = False

                if res == EXIT_STATUS_UNREALIZABLE and is_realizable(t):
                    print ()
                    print (t)
                    print ('FAILED: should be realizable: tool found it unrealizable. hm.')
                    allPassed = False
    if not allPassed:
        exit(1)

    print ('ALL TESTS PASSED')

